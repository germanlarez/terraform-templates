provider "aws" {
  region = "us-west-2"
}

// VPC VARS
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env-prefix {}
variable my_ip {}

// INSTANCE VARS
variable instance_type {}
variable "public_key_location" {}

// VPC

resource "aws_vpc" "test-vpc" {
    cidr_block = var.vpc_cidr_block
    
    tags = {
        Name = "${var.env-prefix}-vpc"
    }
}

// Subnets

resource "aws_subnet" "subnet-01" {
    vpc_id = aws_vpc.test-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    
    tags = {
        Name = "${var.env-prefix}-subnet"
    }
}

// Internet Gateway IGW

resource "aws_internet_gateway" "test-igw" {
    vpc_id = aws_vpc.test-vpc.id

    tags = {
        Name = "${var.env-prefix}-igw"
    }  
}

// Route table for local routes and to Internet 

resource "aws_route_table" "test-route-table" {
    vpc_id = aws_vpc.test-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.test-igw.id
    }  

    tags = {
      Name = "${var.env-prefix}-rtb"
    }
}

// Route table association for subnet to go to igw

resource "aws_route_table_association" "test-rtb-association" {
    subnet_id = aws_subnet.subnet-01.id
    route_table_id = aws_route_table.test-route-table.id
}

// Security Group with just por 22 open to our IP and port 8080 for nginx

resource "aws_security_group" "test-sg" {
    name = "test-sg"
    vpc_id = aws_vpc.test-vpc.id

    ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = var.my_ip
    }

    ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    }

    tags = {
      Name = "${var.env-prefix}-test-sg"
    } 
  
}

// EC2 Instance
// data source for getting the latest image

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]

    filter {
      name = "name"
      values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
    }
  
}

/* OUTPUT to test the value of data source
output "aws_ami_id" {
    value = data.aws_ami.latest-amazon-linux-image.id
}*/ 

// Create a keypair in aws from a local existent key

resource "aws_key_pair" "ssh-key" {
    key_name = "test-key"
    public_key = "${file(var.public_key_location)}"
}

resource "aws_instance" "test-server" {
    
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type
    
    subnet_id = aws_subnet.subnet-01.id
    vpc_security_group_ids = [aws_security_group.test-sg.id]
    availability_zone = var.avail_zone
    
    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    user_data = file("entry-script.sh")

    tags = {
      Name = "${var.env-prefix}-test-server"
    } 
}

output "ec2_public_ip" {
    description = "Public ip to ssh"
    value = aws_instance.test-server.public_ip  
}

